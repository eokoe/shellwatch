$(document).ready(function(){
  console.log("metric-vars: " + metric_vars);

  //EVENTS
  //Open a hook tree node that have ul above it
  $(".node-closed").click(function(event){
    event.stopPropagation();
    var obj = $(this);
    var found = obj.find('UL').toggle('blind');
    //console.log("FOUND: " + found.html());
    //showhideObj(obj.find('UL') ); 
    class_name = obj.attr('class');
    if(class_name.match(/closed/)){
      class_name = class_name.replace('closed','open');
    }
    else if(class_name.match(/open/)){
      class_name = class_name.replace('open','closed');
    }
    obj.attr('class',class_name);

  });

  //Open a code box from hook tree
  $(".anchor-code").click(function(event){
    event.stopPropagation();
    var obj = $(this);
    var action = obj.attr('action');
    if(action == 'open-hook'){
      event.stopPropagation();
      obj.parent().parent().find('PRE').toggle('fade') ; 
    }
    else if (action == 'delete-hook'){
      var hook_name = obj.attr('hook');
      var host = obj.attr('host');
      var metric = obj.attr('metric');
      var hook_li = obj.parent().parent();
      console.log('DELETE HOOK > HOST: ' + host); 
      console.log('DELETE HOOK > METRIC: ' + metric); 
      console.log('DELETE HOOK > HOOK_NAME: ' + hook_name); 
      delete_hook( hook_li, host, metric, hook_name );
    }
    else {
      console.log('Invalid action! - ' + action );
    }
  });



  //Click in 'My hooks'
  $('#root').click(function(event){
    event.stopPropagation();
    $('#hook-container').toggle('blind');
  });

  // Add/Save Hook
  //Click to show Save hook form
  $('#btn-add-hook').click(function(event){
    $('#dialog-form').toggle('fade');
    $('#txt-custom-name').focus();
  });


  //Click to save hook
  $('#btn-save-hook').click(function(event){
    //mounting url for save hook
    
    host = $(this).attr('host');
    metric_name = document.getElementById('hd-metric-add-hook').value;
    metric_period = document.getElementById('txt-period-add-hook').value;
    identifier = document.getElementById('hd-hook-identifier').value;
    bash_code = $('#fpre').text();
    console.log('BASH CODE: ' + bash_code);
    if(!bash_code){
            $( "#dialog-message").attr('title','Save Hook');
            $( "#dialog-message" ).html('<p>O código está vazio!</p>');
            $(function() {
              $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }
                }
              });
            });
      return false;
    }
    custom_name = document.getElementById('txt-custom-name').value;
    data_to_send = { 
                     'identifier':identifier,
                     'bash-code':bash_code,
                     'custom-name':custom_name,
                     'script-selected':$('#script-selected').attr('value'),
                    };
    url = '/api/hook/' + host + '/' + metric_name + '/' + metric_period;
    $('#form-add-hook').attr('action',url) ;
    console.log('>>> SAVE-HOOK-ACTION: ' + $('#form-add-hook').attr('action') );
    var result;


    $.ajax({
          'type': 'POST',
          'url': url,
          'data': data_to_send,
          'success': function(data,txt_status,jqxhr){
            console.log("SUCCESS(" + txt_status + "): " + data);
            $( "#dialog-message").attr('title','Save Hook');
            $( "#dialog-message" ).html('<p>Save Hook succeeded!</p>');
            $(function() {
              $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                  Ok: function() {
                    location.reload();
                  }
                }
              });
            });
            //location.reload();
            return data;
          },
          'error': function(jqxhr,txt_status,error_thrown){
            console.log("ERROR: " + txt_status );  
            $( "#dialog-message").attr('title','Save Hook NOT OK!');
            var data = {}; //JSON.parse(jqxhr.responseText);
            try { data = JSON.parse(jqxhr.responseText); }
            catch(err){
              console.error('ERROR JSON PARSE: ' + err);
              data = { 'message':"Can't save hook!"  };
            }

            console.log("ERROR: " + data.message );  
            $( "#dialog-message").attr('title','Save Hook');
            $( "#dialog-message" ).html('<p>' + data.message + '</p>');
            $(function() {
              $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }
                }
              });
            });

          },
          'dataType': 'json'
        });
  });

  
  $('.btn-save-notifier').click(function(event){
    var data_to_send={};
    var obj = $(this);
    
    var host = obj.attr('host');
    var metric = obj.attr('metric');
    
    //mounting url
    var url = '/api/notifiers/' + host + '/' + metric;

    /*Getting notifiers metadata*/
    var notifiers = {};
    var Levels = ['CRITICAL','WARNING','UNKNOWN'];//por enquanto. So pra testes.
    var Types = ['email'];
    var notifier_defs = ['responsability','firstdelay','interval','recovery','delay'];
    for(l in Levels){
      level = Levels[l];
      notifiers[level] = {};
      for(t in Types){
        type = Types[t];
        notifiers[level][type] = {};
        for(index in notifier_defs){
          obj_name = notifier_defs[index];
          obj_id = metric + '-' + type + '-'+ obj_name + '-' + level;
          obj = document.getElementById(obj_id);
          console.log('id..: ' + obj_id);
          console.log('value...: ' + $('#' + obj_id).value );
          if(obj){ 
            notifiers[level][type][obj_name] = obj.value;
            var output = '';
            for (var property in obj) {
              output += property + ': ' + obj[property]+'; ';
            }
            //console.log("DUMP: " + output);
          }
          else {
            console.log("HOUSTON, WE HAVE A PROBLEM!");
          }
        }
      }
    }
    
    data_to_send['notifiers'] = JSON.stringify(notifiers);
    console.log('NOTIFIERS: ' + data_to_send['notifiers'] );
    $.ajax({
         'type': 'POST',
         'url': url,
         'data': data_to_send,
         'success': function(data,txt_status,jqxhr){
           console.log("SUCCESS(" + txt_status + "): " + data);
           $( "#dialog-message").attr('title','Save Notifier');
           $( "#dialog-message" ).html('<p>Save Notifier OK!</p>');
           $(function() {
             $( "#dialog-message" ).dialog({
               modal: true,
               buttons: {
                 Ok: function() {
                   $( this ).dialog( "close" );
                 }
               }
             });
           });
         },
         'error': function(jqxhr,txt_status,error_thrown){
           var data = {};//JSON.parse(jqxhr.responseText);
           try { data = JSON.parse(jqxhr.responseText); }
            catch(err){
              console.error('ERROR JSON PARSE: ' + err);
              data = { 'message':"Can't save notifiers!"  };
            }
           console.log("ERROR: " + data.message );  
           $( "#dialog-message").attr('title','Save notifier NOT OK!');
           $( "#dialog-message" ).html('<p>' + data.message + '</p>');
           $(function() {
             $( "#dialog-message" ).dialog({
               modal: true,
               buttons: {
                 Ok: function() {
                   $( this ).dialog( "close" );
                 }
               }
             });
           });
         },
         'dataType': 'json'
       });
  });





  //Show hide notifier form
  $('.leaf').click(function(event){
    var obj = $(this);
    var metric = obj.attr("metric-name");
    var host = obj.attr("host");
    var id = 'notifiers-' + metric;
    $('#' + id).toggle('fade');
    load_notifiers(obj);
  });


  //Treating when focus out from custom name textbox
  $('#txt-custom-name').focusout(function(event){
      text = document.getElementById('txt-custom-name').value;
      if(text.length == 0){
        $('#txt-custom-name').css('border','2px solid red;');
        return false;
      }
  }); 
  

  //Treating identifier text  
  $('#script-identifier-title').keydown(function(event){
    var key = event.which;
    if(key == 13){ //enter
      var text = document.getElementById('txt-custom-name').value;
      console.log("TEXT: " + text);
      if(text.length == 0){
        $('#txt-custom-name').css('border','2px solid red;');
        return false;
      }
    }
  });
  
  //Treating the metric choice
  $('#dropdownMenu1').click(function(event){
    $('.menu-metric-container').toggle('blind');
  });
  
  $('.menu-metric-item').click(function(event){
    metric = $(this).html();
    host = $(this).attr('host');
    $('#hd-metric-add-hook').attr('value',metric);
    $('#dropdownMenu1').html(metric + '&nbsp;<span class="caret"></span>' );
    console.log('METRIC - ' + metric);
    console.log('aqui');
    $('.menu-metric-container').slideUp('slow');
    $('#txt-period-add-hook').focus();
    
    //creating available variables list.
    var metric_list = '<ul style="list-style-type: none; padding-left: 1em;">';
    var raw_list = metric_vars[metric];
    metric_list += '<li class="tiny" style="margin-left: -13px;">The metric information are available in the exported variables referenced in the blue boxes at bellow. You can use it! Ex: "$' + raw_list[0] + '": </li>';

    for(v in raw_list){
      console.log('li ' + raw_list[v]);
      metric_list += '<li style="display: inline-block; width: 70px; border: 1px double #f1f1f1; background-color: #337ab7; color:#f1f1f1; padding-top: 5px; padding-bottom: 5px; text-align: center; " >' + raw_list[v] + '</li>';
    }
    metric_list += '</ul>';

    //creating the metric script list
    console.log("HOST: " + host);
    console.log("METRIC: " + metric);
    build_hook_tree(host,metric) ;

    //show
    $('#metric-vars').html(metric_list);
    $('#div-period').fadeIn('slow');

  });



  //this turns on the highlight from hook script box.
  var enterflag=0;
  $('#fpre').keydown(function(evt){
    key = evt.which;
    if(key == 13){
      var precode = document.getElementById('fpre');
      var current_text = $('#fpre').text;
      enterstr="\n\n";
      console.log('FLAG: ' + enterflag);
      $('#fpre').text(function(index,text){ $('#fpre').text(text + enterstr);  })
      Prism.highlightElement(precode,true);
      //$('#fpre').height(function(index,height){  return(height + 50);});
      
    }
  });


  //show options on metric hook creation box
  var txt_show_flag = 0; 
  $('#txt-period-add-hook').keydown(function(evt){
    hookname = document.getElementById('txt-period-add-hook').value;
    if(hookname.length > 0 && $(this).val().match(/^[0-9]+$/) ){
      console.log('input ok!');
      $('#choose-list').fadeIn('slow');
      txt_show_flag = 1;
    }
    else {
      console.log('input not ok!');
      $('#choose-list').fadeOut('slow');
    }
  });



  /* CREATE HOOK NAVIGATION */

  //treat option 1 from metric hook creation box
  var Opt_flags=[];
  $('#hook-opt-1').click(function(){
    treat_opt(1,false);
  });

  //treat back from option 1 
  $('#link-back-opt1').click(function(){
    Opt_flags[1] = 0;
    treat_opt(1,true);
  });


  //treat option 2 from metric hook creation box
  Opt_flags[2] = 0;
  $('#hook-opt-2').click(function(){
    treat_opt(2,false);
  });

  //treat back from option 2 
  $('#link-back-opt2').click(function(){
    opt2_flag = 0;
    treat_opt(2,true);
  });


  //treat option 3 from metric hook creation box
  $('#hook-opt-3').click(function(){
    console.log('treating opt3');
  });


  /* END CREATE HOOK NAVIGATION */





//END EVENTS



//FUNCTIONS
  
    //click on Delete Hook link
  function delete_hook( li,host,metric,hook_name ){
    var url = '/api/hook/' + host + '/' + metric + '/' + hook_name; 
    $.ajax({
        'type': 'DELETE',
        'url': url,
        'success': function(data,txt_status,jqxhr){
          console.log("SUCCESS(" + txt_status + "): " + data);
          li.toggle('fade');//hides <li> that contains the hook information.
          return data;
        },
        'error': function(jqxhr,txt_status,error_thrown){
          console.log("ERROR: " + txt_status );  
          var data = {};
          try { data = JSON.parse(jqxhr.responseText); }
          catch(err){
            console.error('ERROR JSON PARSE: ' + err);
            data = { 'message':"Can't delete hook!"  };
          }
          console.log("ERROR MESSAGE: " + data.message );  
          $( "#dialog-message").attr('title','Delete hook');
          $( "#dialog-message" ).html('<p>' + data.message + '</p>');
          $(function() {
            $( "#dialog-message" ).dialog({
              modal: true,
              buttons: {
                Ok: function() {
                  $( this ).dialog( "close" );
                }
              }
            });
          });
          return false;
        },
    });
  }
  
  function showhide(id){
    var obj = $('#' + id);
    if(obj.css('display') == 'none' ){
      obj.show();
    }
    else {
      obj.hide();
    }
  }

  function showhideObj(obj){
    if(obj.css('display') == 'none' ){
      obj.show();
    }
    else {
      obj.hide();
    }
  }


  function load_notifiers(obj){
    host = obj.attr('host');
    metric = obj.attr('metric-name');
    console.log('LOADING NOTIFIERS OF METRIC ' + metric);
    var url='/api/notifiers/' + host + '/' + metric;
    $.ajax({
      'type': 'GET',
      'url': url,
      'success': function(data,txt_status,jqxhr){
        console.log("SUCCESS(" + txt_status + "): " + JSON.stringify(data) );
        //put values in your places
        Text = $('#form-notifier-' + metric ).children().find('input');
        //console.log('FORM...: ' + form[0].id );
        console.log('LENGTH: ' + Text.length)
        for(i=0;i < Text.length;i++){
          if(Text[i].type == 'text' ){
            Items = Text[i].id.split(/-/);
            engine = Items[1];
            engine_var = Items[2];
            notifier_type = Items[3];
            Text[i].value = data[host][metric][notifier_type][engine][engine_var];
          }
        }
        return data;
      },
      'error': function(jqxhr,txt_status,error_thrown){
        console.log("ERROR: " + txt_status );  
        var data = {}; //JSON.parse(jqxhr.responseText);
        try { data = JSON.parse(jqxhr.responseText); }
        catch(err){
          console.error('ERROR: ' + err);
          data = { 'message':"Can't load notifier for " + metric + "!"  };
        }
        console.log("ERROR: " + data.message + 'status(' + jqxhr.status + ')' );  
      },
      'dataType': 'json'
    });
  }

  $('#script-select').click(function(){
    var script_name = $('#script-select option:selected').text() ;
    var value = $('#script-select option:selected').val() ;
    console.log('SCRIPT_NAME: ' + script_name);
    console.log('VALUE: ' + value);
    if( value != 'undefined' && value != 'no-choice'){
      console.log('IF: ' + script_name);
      $('#btn-save-hook').css('display','inline');
      $('#npre').fadeIn('slow');
    }
    else {
      console.log('ELSE: ' + value);
      $('#btn-save-hook').css('display','none');
    }
  });


  function build_hook_tree(host,metric){
    var url='/api/hook_tree/' + host;
    var opts = '';
    $.ajax({
      'type': 'GET',
      'url': url,
      'success': function(data,txt_status,jqxhr){
        var length = Object.size( data[host][metric] ) ;
        select = '<optgroup label="' + metric + '">';
        for(i=0; i < length; i++){  
          hook = data[host][metric][i];
          select += '<option value="" >' + hook.identifier + '</option>'; 
        }
        select += '</optgroup>';
        $('#script-select').append(select);

        //place bash code from selected script in new script on 'fpre' to save.
        $('#fpre').text(hook.bash_template);
        
        //place bash code from selected script in new script on 'npre' to show.
        $('#npre').text(hook.bash_template);
      },
      'error': function(jqxhr,txt_status,error_thrown){
        console.log("ERROR: " + txt_status );  
        var data = {}; //JSON.parse(jqxhr.responseText);
        try { data = JSON.parse(jqxhr.responseText); }
        catch(err){
          console.error('ERROR: ' + err);
          data = { 'message':"Can't load hooks from " + host + "!"  };
        }
        console.log("ERROR: " + data.message + 'status(' + jqxhr.status + ')' );  
      },
      'dataType': 'json'
    });
    return opts;
  }



  function treat_opt(opt,reverse) {
    console.log('treating opt1');
    if(!Opt_flags[opt]){
      switch(opt){
      case 1 :
        if(!reverse){
          $('#choose-list').fadeOut('slow');
          $('#container-select').fadeIn('slow'); 
        }
        else {
          $('#choose-list').fadeIn('slow');
          $('#container-select').fadeOut('slow'); 
        }
        break;
      case 2 :
        if(!reverse){
          $('#choose-list').fadeOut('slow');
          $('#div-fpre').fadeIn('slow');
          $('#btn-save-hook').fadeIn('slow');
        }
        else {
          $('#choose-list').fadeIn('slow');
          $('#div-fpre').fadeOut('slow');
          $('#btn-save-hook').fadeOut('slow');
        }
        break;
      };
    }
  }
 
  Object.size = function(obj) {
      var size = 0, key;
      for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
      }
      return size;
  };


});


