#!/bin/bash

# Copyright (c) 2015 Thiago Rondon. <thiago@eokoe.com>
# http://www.eokoe.com/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Args
API=$1
DEBUG=1

if [ ! $API ] ; then
	echo "$0 http://URL_API:PORT"
	exit 1
fi

## Commands
CMD_CURL='/usr/bin/curl'
CMD_CAT='/bin/cat'
CMD_VMSTAT='/usr/bin/vmstat'
CMD_DATE='/bin/date'
CMD_DF='/bin/df'
CMD_SENDMAIL='/usr/sbin/sendmail'

## Email VARS
EMAIL_FROM='suporte@eokoe.com'

#this must be the administrator e-mail
EMAIL_TO='andre.carneiro@eokoe.com'
EMAIL_SBJ='Eokoe-ALERT: '


_verify_if_can_exec() {
	if [ ! -x $1 ] ; then
		echo "$1 not found"
		exit 1
	fi
}

_verify_if_can_exec $CMD_CURL
_verify_if_can_exec $CMD_CAT
_verify_if_can_exec $CMD_VMSTAT
_verify_if_can_exec $CMD_DATE
_verify_if_can_exec $CMD_DF

## Files
PROC_LOADAVG='/proc/loadavg'
PROC_MEMINFO='/proc/meminfo'
ETC_HOSTNAME='/etc/hostname'

_verify_if_can_read() {
  if [ ! -r $1 ] ; then
		echo "$1 not found"
		exit 1
	fi
}

_verify_if_can_read $PROC_LOADAVG
_verify_if_can_read $PROC_MEMINFO
_verify_if_can_read $ETC_HOSTNAME


## Outputs
LOADAVG=`$CMD_CAT $PROC_LOADAVG`
VMSTAT=`$CMD_VMSTAT`
MEMINFO=`$CMD_CAT $PROC_MEMINFO`
HOST=`$CMD_CAT $ETC_HOSTNAME`
TIME=`$CMD_DATE +"%s"`
DF=`$CMD_DF`

#login...
CMD="$CMD_CURL -L -X POST  -c ./cookie -d 'username=$SWUSER&password=$SWPASSWORD' '$API/login'"
	eval $CMD;

_curl_post() {

  CMD="$CMD_CURL -L -X POST -b ./cookie '$API/host/$HOST/$1&time=$TIME'"
  	if [ $DEBUG -eq 1 ]; then
		echo $CMD;
		eval $CMD;
	else	
		eval $CMD >/dev/null 2>&1
	fi
}

# UPTIME, TODO: services
_send_uptime() {
	_curl_post "uptime?extra=machine&service=machine&up=1"
}

# Loadaverage is the system load, which is a measure of the amount
# of computational work that a computer system performs.
_send_loadavg() {
	loadavg1=`echo $LOADAVG | tr -s ' ' | cut -d ' ' -f 1`
	loadavg5=`echo $LOADAVG | tr -s ' ' | cut -d ' ' -f 2`
	loadavg15=`echo $LOADAVG | tr -s ' ' | cut -d ' ' -f 3`
	loadavgp=`grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage ""}'`
	
  echo "loadavgp: $loadavgp"
  _curl_post "loadavg?cpuload=$loadavgp&avg1=$loadavg1&avg5=$loadavg5&avg15=$loadavg15"
}

# Get memory stats
_get_from_meminfo() {
    echo "$MEMINFO" | grep ^$1: | sed -e 's/'$1':\s*\([0-9]*\).*$/\1/'
}

_send_meminfo() {
	mem_free=`_get_from_meminfo "MemFree"`
	mem_free=`expr $mem_free \* 1024`
	mem_total=`_get_from_meminfo "MemTotal"`
	mem_total=`expr $mem_total \* 1024`
	mem_cached=`_get_from_meminfo "Cached"`
	mem_cached=`expr $mem_cached \* 1024`
	mem_buffers=`_get_from_meminfo "Buffers"`
	mem_buffers=`expr $mem_buffers \* 1024`

	_curl_post \
		"memory?total=$mem_total&free=$mem_free&cached=$mem_cached&buffers=$mem_buffers"
}

_send_network_dev() {
	for device in $(ls /sys/class/net/); do
		tx=`$CMD_CAT /sys/class/net/$device/statistics/tx_bytes`
		rx=`$CMD_CAT /sys/class/net/$device/statistics/rx_bytes`

		_curl_post "network?extra=$device&dev=$device&txbytes=$tx&rxbytes=$rx"

	done
}

_send_df_dev() {
  IFS=$'\n'
  flag=0
  #echo "COMMAND: $CMD_DF" 
  devs=`$CMD_DF -T --exclude-type=devtmpfs --exclude-type=tmpfs | awk '{print $7 "  " $3 " " $4 " " $5}' `
  echo "DEVS: $devs" > /tmp/df.tmp

  flag=0
  while read line;do 
    #ignorando a primeira linha
    if [ "$flag" = 0 ] ; then
      flag=1
      continue
    fi
	  
    #setando as metricas
    device=`echo "$line" | awk '{print $1}'`
	  total=`echo "$line" | awk '{print $2}'`
	  used=`echo "$line" | awk '{print $3}'`
	  available=`echo "$line" | awk '{print $4}'`
    ndevice=`echo $device |sed 's#/##'`
    if [ ${#ndevice} == 0 ]; then
      ndevice='slash'
    fi

    total=`expr $total`
    available=`expr $available`
    used=`expr $used`
    
    _curl_post "df?extra=$ndevice&dev=$ndevice&available=$available&used=$used&total=$total"
  
  done < /tmp/df.tmp
}

# Usage
SCRIPT_NAME=${0##*/} 
usage() {
	echo "Usage: $SCRIPT_NAME [options]"
	echo "Options:"
	printf "    %-28s %s\n" "-h|--help" "Displays detailed usage information."
	printf "    %-28s %s\n" "--debug" "Displays information for debugging."
	printf "    %-28s %s\n" "--loadavg" "Reports load average stats."
	printf "    %-28s %s\n" "--meminfo" "Reports memory metrics."
	printf "    %-28s %s\n" "--network" "Reports network information about all interface."
	printf "    %-28s %s\n" "--df" "Reports free disk space about all filesystems."
	printf "    %-28s %s\n" "--uptime" "Reports how long system has been running."
}

SHORT_OPTS="h"
LONG_OPTS="help,debug,loadavg,meminfo,network,df,uptime,user:,password:"

ARGS=$(getopt -s bash --options $SHORT_OPTS --longoptions "$LONG_OPTS" --name $SCRIPT_NAME -- "$@" ) 

eval set -- "$ARGS" 
while true; do 
	case $1 in
		# General
	    	-h|--help) 
			usage 
			exit 0 
			;;
		--debug)
			DEBUG=1
			;;
		--loadavg)
			_send_loadavg
			;;
		--meminfo)
			_send_meminfo
			;;
		--network)
			_send_network_dev
			;;
		--df)
      _send_df_dev
			;;
		--uptime)
			_send_uptime
			;;
		--user)
    echo "USER:$2!"
			;;
		--password)
    echo "PASSWORD: $2!"
			;;


		*)
			shift
			break
			;;
	esac
	shift
done

