use common::sense;

package ShellWatch::Notifier;
use Moose;
use namespace::autoclean;
use FindBin qw/$Bin/;
use Config::General;
use File::Path qw/make_path/;
use JSON qw/encode_json decode_json/;
with 'MooseX::Traits';

use Data::Dumper;
use JSON qw/encode_json decode_json/;

has [qw/host metric/] => (
  is       => 'ro',
  isa      => 'Str',
  required => 1
);

has status => (
  is       => 'rw',
  default  => 'UNKNOWN',
  required => 1,
);

has 'base_dir' => (
  is      => 'rw',
  isa     => 'Str',
  default => sub { join '/', "$Bin/../", 'meta'; }
);

has [qw/error name json/] => (
  is      => 'rw',
  isa     => 'Str',
  default => '',
);

has [qw/config data/] => (
  is  => 'rw',
  isa => 'HashRef',
);

has generic_config => ( is => 'rw', );

has message => (
  is  => 'rw',
  isa => 'Str',
  default =>
q{This message means that server couldn't be monitored properlly. Please check hooks scripts return messages!},
);

has full_config => (
  is  => 'rw',
  isa => 'Config::General',
);


sub BUILD {
  my ($self) = @_;

  my $config_dir = join( '/', "$Bin/..", 'etc' );
  my $target = join( '/', $config_dir, 'notifiers.conf' );
  if ( -e $target ) {
    my $obj = Config::General->new(
      -ConfigFile      => $target,
      -InterPolateVars => 1,
    );
    $self->full_config($obj);
    my $generic_conf = {};
    foreach my $level (qw/CRITICAL WARNING UNKNOWN/) {
      $generic_conf->{$level} =
        $obj->{config}->{generic}->{ lc($level) }->{ $self->type };
    }
    $self->generic_config($generic_conf);

    if ( defined( $self->metric )
      && $self->metric
      && exists( $obj->{config}->{ $self->host }->{ $self->metric }->{CRITICAL} )
      && exists( $obj->{config}->{ $self->host }->{ $self->metric }->{WARNING} )
      && exists( $obj->{config}->{ $self->host }->{ $self->metric }->{UNKNOWN} )
      )
    {
      my $metric_conf = {};
      foreach my $level (qw/CRITICAL WARNING UNKNOWN/) {
        $metric_conf->{$level} =
          $obj->{config}->{ $self->host }->{ lc($level) }->{ $self->type };
      }
      $self->config($metric_conf);
    }
    else {
      $self->config($generic_conf);
    }
  }
  else {
    $self->error('Notifier config file was not found!');
  }
}

sub add {
  my ($self) = @_;
  my $ok     = 0;
  my $ext    = '.notifiers';
  my $dir   = join '/',$self->base_dir, $self->host, $self->metric;
  make_path $dir if !-d $dir; #Creates dir automaticaly if doesn't exists.
  my $filename =
    join( '/', $dir, $self->metric )
    . $ext;

  if ( $self->_validate_data ) {    #_check_data MUST be implemented on traits!
    if ( !open my $fh, '>:encoding(UTF-8)', $filename ) {
      $self->error("Can't create filename '$filename'");
      say $self->error . " - $!";
    }
    else {
      my $all_notifiers = decode_json( $self->json );

      #interpolating groups when is necessary
      my $conf = $self->full_config->{DefaultConfig};
    mainloop: foreach my $level (qw/CRITICAL WARNING UNKNOWN/) {
        foreach my $type (qw/email/) {
          my $fields = $all_notifiers->{$level}->{$type};
          foreach my $key ( keys %{$fields} ) {
            if ( $fields->{$key} =~ /\$(.*)$/ ) {
              my $group = $1;
              if ( $conf->{$group} ) {
                $fields->{$key} = $conf->{$group};
              }
              else {
                $self->error(qq/Group '$group' is invalid!/);
                last mainloop;
              }
            }
          }
          $all_notifiers->{$level}->{$type} = $fields;
        }
      }
      if ( !$self->error ) {
        my $json = encode_json $all_notifiers;
        print $fh $json;
        close $fh;
        $ok = 1;
      }
    }
  }
  else {
    $self->error('Invalid data!') if !$self->error;
  }
  return $ok;
}

sub load_config_from_metric {
  my ($self) = @_;
  my ( $host, $metric ) = ( $self->host, $self->metric );
  my $data;
  my $ext = '.notifiers';
  my $filename =
    join( '/', $self->base_dir, $self->host, $self->metric, $self->metric )
    . $ext;

  say "METRIC NOTIFIER FILE: $filename";
  if ( -e $filename ) {
    if ( !open my $fh, '<:encoding(UTF-8)', $filename ) {
      say "PROBLEMS! $!";
    }
    else {
      my $lines = '';
      while (<$fh>) {
        $lines .= $_;
      }
      close $fh if defined $fh;
      eval { $data = decode_json($lines); };
      if ( ref($data) !~ /HASH/ ) {
        say "Oh no... is not a hash!";
      }
      else {
        my $config = {};
        foreach my $level (qw/CRITICAL WARNING UNKNOWN/) {
          $config->{$level} = $data->{$level}->{ $self->type };
        }
        $data = $config;
        undef $config;
      }
    }
  }
  say "NOTIFIER DATA: " . Dumper $data;
  $self->data($data) if defined $data and ref($data) =~ /HASH/;
  return $data;
}

__PACKAGE__->meta->make_immutable;

=pod

=encoding utf- 8

=head1 NAME

  ShellWatch::Notifier

=head1 DESCRIPTION

  This module controls generic attributes and actions
  for ShellWatch Notification System(SWNS) .

=head2 Introduction

SWNS is composed by three components:

=over

=item * shellwatch webapp endpoint The endpoint is 'POST /host/:host/:type' 
For more details see ROUTES session in shellwatch module perldoc

=item * ShellWatch::Notifier - I know that you know that we're talking about this module right now!


=item * ShellWatch::Notifier::<trait module> - Is trait module for treat notification data before data sending and implementation of sending itself. 
For further details please RTFM on ShellWatch::Notifier::<trait module>

=back

SWNS depends totally of metric data and/or 'heartbit' data from client hosts. Metric data are
sending by agent script that is located on client. This agent harvests all metrics from client host
and sending following a pattern similar to Nagios message pattern. Ex: LOADAVG CRITICAL - "last loadavg: 10". RTFM ShellWatch::Script

The webapp shellwatch has the endpoint(decribed above) to receive this data and treat properly.

'Heartbit data' is collected by Shellwatch itself(out of server). One of tasks of shellwatchd is collect heartbit info
from every single server. As the client agent must send data, if the registry of this sending is no longer
updated, shellwatch automatically send notification about it. NOT IMPLEMENTED YET... 


=head2 Configuring notifiers

As there are various types of notification, but this module concentrate common attributes and method and gives
specific traits for specific 'trait classes'.  Ex: ShellWatch::Notifier::email

The instance try to load notifier data automatically from two sources: 

=over

=item * The metric notification file(Ex: loadavg.notifiers): Is created by user through ShellWatch web interface;

=item * The default notification file: Ie, 'notifiers.conf'. This file contain the default notification for some metric, or for all metric. Besides
 this file contains group of recipients for a notification and what type of notifications are active. Ex:

  #active_notifiers(separated by space)
    active_notifiers email
  
  #groups(separated by comma)
    grp1  andre.carneiro@eokoe.com
  
  #default email target(recipient)
    default_target = andre.carneiro@eokoe.com
  
  #default for ANY metric and ANY host
    <generic>
      <critical>
        <email>
          responsability $default_target
          notification_interval 10
        </email>
      </critical>
      
      <warning>
        <email>
          responsability $default_target
          notification_interval 10
        </email>
      </warning>
  
      <unknown>
        <email>
          responsability $default_target
          notification_interval 10
        </email>
      </unknown>
    </generic>
  
  #host/metric/notification/level
    <horrorscope>
      <loadavg>
        <critical>
          <email>
            responsability $grp1
            first_delay 10
            notification_interval 10
            notification_recovery 0
          </email>
        </critical>
  
        <warning>
          <email>
            responsability $grp1
            first_delay 10
            notification_interval 10
            notification_recovery 0
          </email>
        </warning>
  
        <unknown>
          <email>
            responsability $grp1
            first_delay 10
            notification_interval 10
            notification_recovery 0
          </email>
        </unknown>
      </loadavg>
    </horrorscope>

Note that notification must have minimally to fields to work in ShellWatch: responsability and notification_interval. The 'responsability' field can 
be a single or multiple recipients. The 'notification_interval' field is a number that represents the amount of seconds of waiting to send a 
notification.

It is possible to 'interpolate' variables inside this config file. The rules for that is based on Config::General::Interpolated module. So, RTFM :D

From the point of view of ShellWatch web app there is a template for every single kind of notification. This template must be compatible with 
Template Toolkit template. Normaly they stay in <SHELLWATCH_PATH>/view/notifiers/ . Above an example for email notifier interface.

   
  <ul class="node-closed no-bullet" style="display:none; width: 50%;" >
    <li style="padding-top:20px; padding-bottom: 20px; font-size: 18px; font-weight: bold;">E-mail</li>
    <li>
      <label for="email-responsability-[%level%]">Responsability</label>
      <input type="text" class="form-control" name="email-responsability-[%level%]" id="email-responsability-[%level%]" />
      
      <label for="email-firstdelay-[%level%]">First delay</label>
      <input type="text" class="form-control" name="email-firstdelay-[%level%]" id="email-firstdelay-[%level%]" />

      <label for="email-interval-[%level%]">Notification interval</label>
      <input type="text" class="form-control" name="email-interval-delay-[%level%]" id="email-interval-[%level%]" />

      <label for="email-recovery-[%level%]">Notification recovery</label>
      <input type="text" class="form-control" name="email-recovery-delay-[%level%]" id="email-recovery-[%level%]" />


    </li>
  </ul>


=back




=head1 SYNOPSIS
 
 use strict;
 use warnings;
 use ShellWatch::Notifier;

 my $notifier = ShellWatch::Notifier->new_with_traits(traits => ['email']
      host   => $host,
      metric => $metric,
  );

 ##Common methods(all Notifiers)
 
 #load data from metric
 my $data = $notifier->load_from_metric;

=head1 ATTRIBUTES

=head2 host

String that contains the name of host where the notification comes

=head2 metric

String that contains the name of metric where notification belongs

=head2 base_dir

String that contains the common path for all notification. Normally is <SHELLWATCH_PATH>/meta

=head2 error

String for error messages

=head2 json

JSON string that contain all notifiers data from a metric 

=head2 name

In the message pattern a 

=head2 message

String with the notification message

=head2 status

Status of notification. Must be: CRITICAL WARNING UNKNOWN and OK

=head2 generic_config

Hashref with generic configuration for any kind of notification

=head2 full_config

It is a Config::General object

=head1 METHODS

=head2 add()


=head2 load_config_from_metric()


=head1 AUTHOR

Andre Carneiro L<andre.carneiro@eokoe.com>

=head1 LICENSE

?

=cut

1;

