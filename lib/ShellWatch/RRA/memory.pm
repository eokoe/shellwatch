
package ShellWatch::RRA::memory;
use common::sense;
use Moose::Role;
use Data::Dumper;

has metric_name => ( is => 'ro', isa => 'Str', default => 'memory' );
has metric_description => (
    is  => 'ro',
    isa => 'Str',
    default =>
'Show usage memory. Memory Available is the sum of free, cached and buffer memory.'
);

# TODO: Check the sane of numbers.
has [qw(total available buffers cached free)] => (
    is       => 'rw',
    isa      => 'Int',
    required => 0        # todo: /hosts/host/metric
);

has rrd_file => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub { join( '/', shift->rrd_directory, 'memory.rrd' ) }
);

has vars => (is => 'ro',
             default => sub {return ['total', 'available', 'buffers', 'cached', 'free']},
             );


sub update {
    my $self = shift;
    if ($self->free + $self->cached + $self->buffers > $self->total) {
        die 'How can we have more available memory than total ?'
    }

    $self->rrd->create(
        $self->rrd_file,
        Total     => "GAUGE",
        Available => "GAUGE",
        Free      => "GAUGE",
        Cached    => "GAUGE",
        Buffers   => "GAUGE",
    ) unless -f $self->rrd_file;

    $self->rrd->update(
        $self->rrd_file, time,
        Total     => $self->total,
        Available => $self->free + $self->cached + $self->buffers,
        Free      => $self->free,
        Cached    => $self->cached,
        Buffers   => $self->buffers
    );
}

sub graph {
    my $self = shift;

    my %rtn = $self->rrd->graph(
        height           => $self->graph_height,
        width            => $self->graph_width,
        destination      => $self->graph_directory,
        base             => 1024,
        title            => 'Memory Usage',
        line_thickness   => 2,
        #extended_legend  => 1,
        vertical_label   => 'bytes',
        'units-exponent' => 6,
        rigid            => '',
        'slope-mode'     => '',
#        base             => 1024, # memory 1kb => 1024 bytes.
        #'graph-render-mode' => 'mono',
        sources           => [],
        #sources          => [qw(Total Available Buffers Cached Free)],
#        source_colors    => [qw(00FF00 00D000 CCCCCC 007F00 009D00)],
#        source_drawtypes => [qw(AREA AREA LINE1 LINE1 LINE1)],
        color            => [
            (
                "BACK#2F2F2F",   "CANVAS#333333",
                "SHADEA#CCCCCC", "SHADEB#CCCCCC",
                "FONT#FFFFFF",   "AXIS#FFFFFF",
                "ARROW#FF0000",  "GRID#CCCCCC",
                "MGRID#CCCCCC"
            )
        ],
        'AREA:Total#00FF00:Total' => '',
        'GPRINT:Total:LAST:Last %6.2lf %sbytes' => '',
        'GPRINT:Total:MAX:Max %6.2lf %sbytes' => '',
        'GPRINT:Total:AVERAGE:Avg %6.2lf %sbytes' => '',
        'GPRINT:Total:MIN:Min %6.2lf %sbytes\n' => '',

        'AREA:Available#00D000:Available' => '',
        'GPRINT:Available:LAST:Last %6.2lf %sbytes' => '',
        'GPRINT:Available:MAX:Max %6.2lf %sbytes' => '',
        'GPRINT:Available:AVERAGE:Avg %6.2lf %sbytes' => '',
        'GPRINT:Available:MIN:Min %6.2lf %sbytes\n' => '',

        'AREA:Buffers#009D00:Buffers' => '',
        'GPRINT:Buffers:LAST:Last %6.2lf %sbytes' => '',
        'GPRINT:Buffers:MAX:Max %6.2lf %sbytes' => '',
        'GPRINT:Buffers:AVERAGE:Avg %6.2lf %sbytes' => '',
        'GPRINT:Buffers:MIN:Min %6.2lf %sbytes\n' => '',

        'AREA:Cached#007F00:Cached' => '',
        'GPRINT:Cached:LAST:Last %6.2lf %sbytes' => '',
        'GPRINT:Cached:MAX:Max %6.2lf %sbytes' => '',
        'GPRINT:Cached:AVERAGE:Avg %6.2lf %sbytes' => '',
        'GPRINT:Cached:MIN:Min %6.2lf %sbytes\n' => '',

        'LINE1:Total#00FF00' => '',
        'LINE1:Available#00D000' => '',
        'LINE1:Buffers#009D00' => '',
        'LINE1:Cached#007F00' => '', 
        
        'LINE1:Free#CCCCCC:Free' => '',
        'GPRINT:Free:LAST:Last %6.2lf %sbytes' => '',
        'GPRINT:Free:MAX:Max %6.2lf %sbytes' => '',
        'GPRINT:Free:AVERAGE:Avg %6.2lf %sbytes' => '',
        'GPRINT:Free:MIN:Min %6.2lf %sbytes\n' => '',




    );
}

1;

