
package ShellWatch::RRA::loadavg;
use Moose::Role;

has metric_name => ( is => 'ro', isa => 'Str', default => 'loadavg' );
has metric_description => (
    is  => 'ro',
    isa => 'Str',
    default =>
'Loadaverage is the system load, which is a measure of the amount of computational work that a computer system performs.'
);


has vars => (is => 'ro',
             default => sub{['avg1', 'avg5' ]} ,
             );

has [qw(avg1 avg5)] => (
    is       => 'rw',
    isa      => 'Num',
    required => 0,        
);


has rrd_file => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub { join( '/', shift->rrd_directory, 'loadavg.rrd' ) }
);

sub update {
    my $self = shift;
    $self->rrd->create( $self->rrd_file, 'avg1' => 'GAUGE', 
                                         'avg5' => 'GAUGE',
#                                         'avg15' => 'GAUGE',
    )
      unless -f $self->rrd_file;
    $self->rrd->update( $self->rrd_file, time,
        'avg1' => $self->avg1,
        'avg5' => $self->avg5,
#        'avg15' => $self->avg15,
        );
}

sub graph {
    my $self = shift;

    my %rtn = $self->rrd->graph(
        height           => $self->graph_height,
        width            => $self->graph_width,
        destination      => $self->graph_directory,
        title            => 'Load Average',
        line_thickness   => 2,
        extended_legend  => 1,
        vertical_label   => 'System load',
        #sources          => [qw(avg1 avg5)],
        sources          => [],
        #source_colors    => [qw(ff0000)],
        #source_drawtypes => [qw(AREA AREA)],
        'units-exponent' => '0',
        rigid => "",
        'slope-mode' => '',
        'font-render-mode' => 'light',
        color            => [
            (
                "BACK#2F2F2F",   "CANVAS#333333",
                "SHADEA#CCCCCC", "SHADEB#CCCCCC",
                "FONT#FFFFFF",   "AXIS#FFFFFF",
                "ARROW#FF0000",  "GRID#CCCCCC",
                "MGRID#CCCCCC"
            )
        ],

      'AREA:avg1#FF3300:avg1' => '',
      'GPRINT:avg1:LAST:Last %6.2lf ' => '',
      'GPRINT:avg1:MAX:Max %6.2lf ' => '',
      'GPRINT:avg1:AVERAGE:Avg %6.2lf ' => '',
      'GPRINT:avg1:MIN:Min %6.2lf \n' => '',

      'AREA:avg5#00CC00:avg5' => '',
      'GPRINT:avg5:LAST:Last %6.2lf ' => '',
      'GPRINT:avg5:MAX:Max %6.2lf ' => '',
      'GPRINT:avg5:AVERAGE:Avg %6.2lf ' => '',
      'GPRINT:avg5:MIN:Min %6.2lf\n' => '',
    );
}

1;
