
package ShellWatch::RRA::network;

use Moose::Role;

has metric_name => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'Str',
    default => sub { 'network-' . shift->dev }
);

has metric_description => (
    is      => 'ro',
    isa     => 'Str',
    default => 'RX Bytes and TX Bytes. These indicate the total amount of data that has passed through the Ethernet interface either way.'
);

has dev => (
    is       => 'rw',
    isa      => 'Str',
    required => 0
);

has [qw(txbytes rxbytes)] => (
    is       => 'rw',
    isa      => 'Int',
    required => 0
);

has vars => (is => 'ro',
             default => sub {['txbytes','rxbytes'];} ,
             );



sub rrd_file {
    my $self = shift;
    join( '/',
        $self->rrd_directory, join( '', 'network-', $self->dev, '.rrd' ) );
}

sub update {
    my $self = shift;

    $self->rrd->create(
        $self->rrd_file,
        TXbytes => 'DERIVE',
        RXbytes => 'DERIVE'
    ) unless -f $self->rrd_file;
    $self->rrd->update(
        $self->rrd_file, time,
        TXbytes => $self->txbytes,
        RXbytes => $self->rxbytes
    );
}

sub graph {
    my $self = shift;

    $self->update unless -f $self->rrd_file;

    $self->rrd->graph(
        $self->rrd_file,
        height         => $self->graph_height,
        width          => $self->graph_width,
        destination    => $self->graph_directory,
        vertical_label => 'bytes/sec',

        #sources => [ sort grep(/.X(bytes|packets|errs)/,@keys) ],
        sources          => [qw(TXbytes RXbytes)],
        source_labels    => [qw(transmit recieve)],
        source_drawtypes => [qw(LINE1 LINE1)],
        source_colors    => [qw(00ff00 ffff00)],
        extended_legend  => 1,

        line_thickness   => 2,
        rigid => "",
        'slope-mode' => '',
        'font-render-mode' => 'light',

        color => [
            (
                "BACK#2F2F2F",   "CANVAS#333333",
                "SHADEA#CCCCCC", "SHADEB#CCCCCC",
                "FONT#FFFFFF",   "AXIS#FFFFFF",
                "ARROW#FF0000",  "GRID#CCCCCC",
                "MGRID#CCCCCC"
            )
        ],

    );

}

1;

