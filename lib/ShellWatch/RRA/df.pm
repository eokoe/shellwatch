
package ShellWatch::RRA::df;

use Moose::Role;



has metric_name => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'Str',
    default => sub { 'df-' . shift->dev }
);

has metric_description => (
    is      => 'ro',
    isa     => 'Str',
    default => 'Available partition space'
);

has dev => (
    is       => 'rw',
    isa      => 'Str',
    required => 0
);

has [qw(available used total)] => (
    is       => 'rw',
    isa      => 'Num',
    required => 0
);

has vars => (is => 'ro',
             default => sub {['available', 'used', 'total']},
             );



sub rrd_file {
  my $self = shift;
  return join( '/',
        $self->rrd_directory, join( '', 'df-', $self->dev, '.rrd' ) );
}

sub update {
    my $self = shift;
    my $ok = 1;
    $self->rrd->create(
        $self->rrd_file,
        'available' => 'GAUGE',
        'used' => 'GAUGE',
        'total' => 'GAUGE',
    ) unless -f $self->rrd_file;

    if(defined($self->available) && defined($self->used) && defined($self->total) ){
      $ok = $self->rrd->update(
          $self->rrd_file, time,
          'available' => ( $self->available * 1024  ),
          'used' => ( $self->used * 1024 ),
          'total' => ( $self->total * 1024  ),
      );
    }
    else {
      $ok = 1;
    }
    $ok = 0 if !defined($ok);
    return $ok;
}

sub graph {
  my $self = shift;

  $self->update unless -f $self->rrd_file;
  my $dev = $self->dev;
  $dev =~ s/slash//;#slash foi a 'melhor' designação que eu encontrei para expressar / como um dev.
  
  my %rtn = $self->rrd->graph(
    height           => $self->graph_height,
    width            => $self->graph_width,
    destination      => $self->graph_directory,
    title            => "Partition Space(/$dev)",
    line_thickness   => 2,
    #extended_legend  => 1,
    vertical_label   => 'GB',
    'units-exponent' => 9,
    rigid            => '',
    'slope-mode'     => '',
    base             => (1024), # memory 1kb => 1024 bytes.
    #'graph-render-mode' => 'mono',
    sources           => [],
    #sources          => [qw(Total Available Buffers Cached Free)],
#        source_colors    => [qw(00FF00 00D000 CCCCCC 007F00 009D00)],
#        source_drawtypes => [qw(AREA AREA LINE1 LINE1 LINE1)],
    color            => [
        (
            "BACK#2F2F2F",   "CANVAS#333333",
            "SHADEA#CCCCCC", "SHADEB#CCCCCC",
            "FONT#FFFFFF",   "AXIS#FFFFFF",
            "ARROW#FF0000",  "GRID#CCCCCC",
            "MGRID#CCCCCC"
        )
    ],
    'AREA:total#00FF00:total' => '',
    'GPRINT:total:LAST:Last %6.2lf %sbytes' => '',
    'GPRINT:total:MAX:Max %6.2lf %sbytes' => '',
    'GPRINT:total:AVERAGE:Avg %6.2lf %sbytes' => '',
    'GPRINT:total:MIN:Min %6.2lf %sbytes\n' => '',

    'AREA:available#FFD000:available' => '',
    'GPRINT:available:LAST:Last %6.2lf %sbytes' => '',
    'GPRINT:available:MAX:Max %6.2lf %sbytes' => '',
    'GPRINT:available:AVERAGE:Avg %6.2lf %sbytes' => '',
    'GPRINT:available:MIN:Min %6.2lf %sbytes\n' => '',

    'AREA:used#009D00:used' => '',
    'GPRINT:used:LAST:Last %6.2lf %sbytes' => '',
    'GPRINT:used:MAX:Max %6.2lf %sbytes' => '',
    'GPRINT:used:AVERAGE:Avg %6.2lf %sbytes' => '',
    'GPRINT:used:MIN:Min %6.2lf %sbytes\n' => '',


  );
  
  
}

1;

