package ShellWatch::API;
use Moose;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";
use namespace::autoclean;
use common::sense;
use Dancer2 appname => 'shellwatch';
use JSON qw/encode_json decode_json/;
use ShellWatch::Notifier;
use ShellWatch::ScriptMeta;
use ShellWatch::Script;
use ShellWatch::Util qw(sw_dir);
use shellwatch;
use File::Path qw/make_path/;
use Data::Dumper;
use Carp qw/confess/;
use JSON qw/encode_json decode_json/;
use Log::Log4perl;
use Config::General qw/ParseConfig/;

Log::Log4perl->init( config->{logfile} );
my $logger = Log::Log4perl->get_logger(config->{appname});


our $VERSION = '0.1.5';

has message => (is => 'rw');

has error => (is => 'rw');

has json => (is => 'rw');



prefix '/api';


our $user;

hook before => sub {
  $user = session->{data}->{user};
};

post '/hook/:host/:metric/:period' => sub {
  my $ok = 0;
  my ( $identifier, $host, $metric_name, $metric_period, $metric_dev,
    $bash_code, $custom_name, $script_selected )
    = (
    params->{'identifier'},  params->{'host'}, params->{'metric'},
    params->{'period'},      params->{'dev'},  params->{'bash-code'},
    params->{'custom-name'}, params->{'script-selected'},
    );
  #my $p = params;
  #say "P: " . Dumper $p ;
  #say STDERR "SCRIPT-SELECTED: $script_selected" and return ;

  my ( $status, $message ) = ( 500, 'NOT OK' );
  my $hosts_dir = ShellWatch::Util::sw_dir( 'meta', $host );
  my $ok_dir = 0;
  if ( !-d $hosts_dir ) {
    $logger->info("Trying to create directory '$hosts_dir'") if defined $logger;
    if(! mkdir $hosts_dir){ 
      $message = "Can't create directory - $!";
      $status = 500;
    }
    else {
      $logger->info("Directory '$hosts_dir' created!") if defined $logger;
      $ok_dir = 1;
    }
  }
  else {
    $ok_dir = 1;
  }

  if($ok_dir) {
    my $no_extra = $metric_name;
    $no_extra =~ s/-.*$// if $no_extra =~ /-/;
    my $hooks_dir = ShellWatch::Util::sw_dir( 'hooks', $no_extra );

    my $var_name = params->{ 'metric-' . $metric_name . '-var' };
    $identifier = $custom_name if length($custom_name) > 0;
    my $hook = {
      host       => $host,
      metric     => $metric_name,
      period     => $metric_period,
      identifier => $identifier,
      code       => $bash_code,
    };

    my $metric = shellwatch::_get_metric( $host, $metric_name );

    #getting data from rrd file
    my ( $rrd_data, $env ) = ( $metric->get_RRD_Data($hook), undef, );
    foreach my $k ( keys %{$rrd_data} ) {
      $ENV{$k} = $rrd_data->{ lc($k) };
      $env->{$k} = $ENV{$k};
    }
    $hook->{env} = {};
    $hook->{env} = $env if defined $env and ref $env =~ /HASH/;
    $script_selected = '' if !defined $script_selected;
    my $script = ShellWatch::Script->new( %{$hook}, 
                                          logger => $logger,
                                          user => $user->{user}, 
                                          script_selected => $script_selected  ,
                                          );
    
    if( defined $script->error and $script->error ){
      $message = 'NOT OK - ' . $script->error;
    }
    if ( $script->add ) {
      $logger->info(q{Adding hook SUCCESS!'} . $script->identifier . q{'}) if defined $logger;
      $ok      = 1;
      $message = 'OK';
      $status  = 200;
    }
    else {
      my $identifier = $script->identifier;
      $message .= " - Adding hook FAIL($identifier) - " . $script->error;
      $logger->error($message) if defined $logger;
      $status = 500;
    }
  }
  my $json = encode_json(
    {
      message  => $message,
      resource => 'hook',
      status   => $status,
    }
  );
  say $json;
  status($status);
  content_type 'application/json';
  return $json;
};


del '/hook/:host/:metric/:hook' => sub {
  my ( $host, $metric, $notifiers, $hook ) = (
    params->{'host'},      params->{'metric'},
    params->{'notifiers'}, params->{'hook'}
  );
  $logger->info("Trying to delete hook '$hook'") if defined $logger;
  my ( $message, $status ) = ( qq{Can't remove hook!}, 500 );
  my $hook_params = {
    host       => $host,
    metric     => $metric,
    identifier => $hook,
  };
  my $script = ShellWatch::Script->new( %{$hook_params},
                                        logger=>$logger,
                                        user => $user->{user}, );
  $message .= ' - ' . $script->error
    if defined $script->error and $script->error;

  if ( $script->remove ) {
    $logger->info("Delete hook '$hook' SUCCESS!") if defined $logger;
    $message = 'OK';
    $status  = 200;
  }
  else {
    $message .= ' - ' . $script->error;
    $logger->error("Delete hook '$hook' FAIL!") if defined $logger;
  }

  my $json = encode_json {
    message  => $message,
    resource => 'hook',
    status   => $status,
  };

  say $json;
  status($status);
  content_type 'application/json';
  return $json;
};

post '/notifiers/:host/:metric' => sub {
  my ( $host, $metric, $notifiers ) =
    ( params->{'host'}, params->{'metric'}, params->{'notifiers'} );
  
  my $ok = 0;
  $logger->info("Trying to save notifiers for metric '$metric'") if defined $logger;
  my ( $status, $message ) = ( 500, 'NOT OK' );
  foreach my $name (qw/email/) {
    my $notifiers = ShellWatch::Notifier->new_with_traits(traits => ['ShellWatch::Notifier::' . $name],
      host   => $host,
      metric => $metric,
      json => $notifiers,
    );
    if ( $notifiers->add ) {
      $ok = 1;
    }
    else {
      $message = 'NOT OK - ' . $notifiers->error;
      $logger->info("Trying to save notifiers for metric '$metric' - FAIL - " . $notifiers->error) if defined $logger;
      $status  = 500;
    }
  }
  if ($ok) {
    $message = 'OK';
    $status  = 200;
    $logger->info("Trying to save notifiers for metric '$metric' - SUCCESS") if defined $logger;
  }

  my $json = encode_json(
    {
      message  => $message,
      resource => 'save_notifiers',
      status   => $status,
    }
  );

  say $json;
  status($status);
  content_type 'application/json';
  return $json;
};


#Retrives a JSON with all .
get '/hook_tree/:host' => sub {
  my $host = params->{'host'};
  my $s = {};
  my $status = 200;
  if(!defined($user) || !$user){
    $status = 401;
    $s = encode_json {
      resource => 'hook_tree',
      message => 'Authentication Required!',
      status => $status,
    };
  }
  else {
    $s = shellwatch::_get_hooks($host,$user->{user}); #Dancer SUCKS! 
    say "S: " . Dumper $s;
  }
  status($status);
  return $s;
};

#You can pass a metric name or 'all'.
get '/notifiers/:host/:metric' => sub {
  my ( $host, $metric ) =
    ( params->{'host'}, params->{'metric'} );
  
  #mapping directories 
  my $hook_dir = join '/',"$Bin/..",'meta';
  my $notifiers_struct = {};
  my $found = 0;
  my $ok = 0;
  my $status = 200;
  my $msg = 'OK';
  if($metric eq 'all'){
    while(<$hook_dir/**/**/*>){
      my $file = $_;
      chomp $file;
      next if $file !~ m{($host)/(.*)/(.*\.notifiers)};
      my ($nhost,$nmetric,$notifiers_file) = ($1,$2,$3);
      next if $nhost ne $host || $nmetric ne $nmetric; #just from your own  host/metric
      my $fh;
      if(!open $fh,'<:encoding(UTF-8)',$file){
        $msg = "Couldn't retrieve notifiers's files!";
        $status = 500;
      }
      else {
        my $json_string = '';
        my @lines = <$fh>;
        my $json = decode_json( "@lines" );
        $notifiers_struct->{$nhost}->{$nmetric} = $json;
        $ok = 1;
        $found = 1;
      }
      close $fh if defined $fh;
    }
  }
  else {
    while(<$hook_dir/**/$metric/*>){
      my $file = $_;
      chomp $file;
      next if $file !~ m{($host)/(.*)/(.*\.notifiers)};
      my ($nhost,$nmetric,$notifiers_file) = ($1,$2,$3);
      next if $nhost ne $host || $nmetric ne $nmetric; #just from your own  host/metric
      my $fh;
      if(!open $fh,'<:encoding(UTF-8)',$file){
        $msg = "Couldn't retrieve notifiers's files!";
        $status = 500;
      }
      else {
        my $json_string = '';
        my @lines = <$fh>;
        my $json = decode_json( "@lines" );
        $notifiers_struct->{$nhost}->{$nmetric} = $json;
        $ok = 1;
        $found = 1;
      }
      close $fh if defined $fh;
    }
  }
  my $json;
  if( $ok && ref( $notifiers_struct ) =~ /HASH/ ){
    if(! keys %{$notifiers_struct} ){
      #From etc/notifiers.conf
      my $config_file = join '/',"$Bin../",'etc','notifiers.conf';
      my %Config = ParseConfig( $config_file );
      
      #Trying find default config by host
      my $notifiers_struct = $Config{$host}->{$metric};
      if(! keys %{ $notifiers_struct} ){
        #Getting general default config as last resource
        my $notifiers_struct = $Config{generic};
        $status = 404;
        $json = encode_json { resource => 'notifiers',
                            message => 'Data not found!',
                            status => $status,
                            };
      }
      else {
        $status = 200;
        $json = encode_json $notifiers_struct; 
      }
      status($status);
      
    }
    else {
      $json = encode_json $notifiers_struct;
      status(200);
    }
  }
  elsif( $ok && ref( $notifiers_struct ) !~ /HASH/ ){
    status(500);
    $json = encode_json {
      resource => 'notifiers',
      message => 'INTERNAL SERVER ERROR',
      status => $status,
    };
  }
  elsif(!$ok && ref($notifiers_struct) =~ /HASH/){
    $status = 404;
    $json = encode_json { resource => 'notifiers',
                          message => 'Data not found!',
                          status => $status,
                          };
    status($status);
  }
  content_type 'application/json';
  return $json;

};


__PACKAGE__->meta->make_immutable;



=pod

=encoding utf-8

=head1 NAME

 ShellWatch::API - Routes that provides CRUD for Shellwatch features 

=head1 DESCRIPTION


=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 ROUTES

=head2 post /hook/:host/:metric/:period

 try to save a script based on his host, metric name and periodicity

=head2 delete /hook/:host/:metric/:hook

 try to delete a hook based on host and metric.

=head2 post /notifiers/:host/:metric/:period

 try to save notifiers structure in files per metric. If metric is 'loadavg' a file named 'loadavg.notifiers' will be saved with
 a JSON structure for notifiers in /meta/<HOST>/loadavg.


=head2 get '/notifiers/:host/:metric' 

  Get all notifiers information from specific metric


=head1 METHODS

=head2 add()

 Adds a new script file based on basic attributes(host, metric, identifier and code).

=cut

1;
