package ShellWatch::Daemon;
use Moose;
use namespace::autoclean;
use common::sense;
use RRD::Simple;
use IPC::Cmd qw[run run_forked];
use ShellWatch::Util qw(sw_dir);
use ShellWatch::Notifier;
use ShellWatch::ServerCheck;
use Data::Dumper;
use Log::Log4perl;
use Net::Ping; 
use FindBin qw/$Bin/;
use Config::General qw/ParseConfig/;

has config => (
  is => 'rw',
);

has logger => (
  is => 'rw',
  default => sub {
    Log::Log4perl->init( join('/', $Bin,'../','etc','log.conf') );
    my $logger = Log::Log4perl->get_logger( $0 );   
    return $logger;
  },
);

our $VERSION = '0.4.0';
our $config;
our @Notification_engines = ('ShellWatch::Notifier::email'); #actually this is useful as a default.

sub BUILD {
  my ($self,) = @_;
  
  my $notification_config_file = join '/',"$Bin/../etc",'notifiers.conf';
  my %c = ParseConfig(  -ConfigFile => $notification_config_file, 
                        -ForceArray => 1,
                    );
  $config = \%c;

  @Notification_engines = $config->{engines}->[0];

}


# Get the RRD file in cache/ based on the script basedir.
sub _rrd_file {
  my $script    = shift;
  my $meta_dir  = sw_dir('meta');
  my $cache_dir = sw_dir('cache');

  $script =~ s/^$meta_dir//;
  $script =~ s/^\///;
  my ( $host, $user, $metric ) = split( /\//, $script );
  my $cache_file = join( '/', $cache_dir, $host, "$metric.rrd" );
  return $cache_file;
}

# Set ENV variables based on rrd info
# Warning: RRD not considered in some cases the last entered value, based on the step.
sub _set_env_by_rrd() {
  my $cache = shift;
  my $rrd   = RRD::Simple->new(
    file => $cache,
  );

  my $info = $rrd->info;
  foreach my $source ( @{ $rrd->sources } ) {
    $ENV{ lc($source) } = $info->{ds}->{$source}->{last_ds};
  }
}

# Run the script
sub _run_script {
  my ($self,$script) = @_;
  &_set_env_by_rrd( &_rrd_file($script) );
  my @Parts = split /\//, $script;
  pop @Parts;
  my ( $metric, $user, $host ) = ( pop @Parts, pop @Parts, pop @Parts );
  @Parts = ();
  my $buffer;
  say "Running '$script'";
  run( command => $script, verbose => 1, buffer => \$buffer );
  $self->_run_notify( $buffer, $host, $metric );
}

sub _parse_nagios() {
  my $output = shift;
  my @Result = ();
  chomp($output);
  if ( $output !~ /^.+? (OK|CRITICAL|WARNING|UNKNOWN).*?-.+?$/ ) {
    warn "invalid hook return! '$output'";
  }
  else {
    my ( $check_result, $check_message ) = split( '-', $output, 2 );
    my ( $check_name, $check_status ) = split( ' ', $check_result );
    @Result = ( $check_name, $check_status, $check_message );
  }
  return @Result;
}

sub _run_notify {
  local $| = 1;
  my ( $self,$output, $host, $metric ) = @_;
  #TODO treat this properly!
  if    ( !defined($host)   || !$host )   { 
    die 'No host!'  
  }
  elsif ( !defined($metric) || !$metric ) { 
    die 'No metric!';  
  }
  else {
    my @Result = &_parse_nagios($output);
    if (@Result) {
      my ( $name, $status, $message ) = @Result;
      $message = $message ? $message : '';
      $status  = $status  ? $status  : 'UNKNOWN';
      if(!@Notification_engines){
        say "Sorry! There is no notification engines configurated!";
      }
      else {
        my $notifier = ShellWatch::Notifier->new_with_traits(
          traits  => @Notification_engines ,
          host    => $host,
          metric  => $metric,
          status  => $status,
          message => $message,
          logger => $self->logger,
        );
        open my $ft, '>', '/tmp/notifier-tmp';
        close $ft;
        my $metric_config_file =
          join( '/', $notifier->base_dir, $host, $metric, $metric )
          . '.notifiers';

        if ( -e $metric_config_file ) {
          say "Loading config from metric '$metric'";
          my $config_data = $notifier->load_config_from_metric;
          $notifier->config($config_data) if $config_data =~ /HASH/;
        }
        say "NOTIFYING...$status";
        $notifier->notify() if defined $status and $status and $status ne 'OK';
      }
    }
  }
}

# Write the epoch time, before run the script.
sub _do_last_run() {
  my ($self,$last_run) = @_;
  open( FH, '>', $last_run ) or die $@;
  print FH time;
  close FH;
}

# Check if we need to run, based on the period set by the metric.
sub _need_to_run() {
  my ( $self,$script, $period, $lastrun ) = @_;
  return 1 unless -f $lastrun;
  my $run_at   = do { local ( @ARGV, $/ ) = $lastrun; <> };
  my $interval = do { local ( @ARGV, $/ ) = $period;  <> };
  return 1 if ( time - $run_at ) > $interval;
  return 0;
}

#main while for check and fork scripts runnings
sub check_all_scripts {
  my ($self) = @_;
  my $dir = sw_dir('meta');
  $0 = 'shellwatch-scriptrunner';
  while ( my $script = glob "$dir/*/*/*/*sh" ) {
    my $period = $script;
    $period =~ s/\.sh$/\.period/;
    warn "Where is $period ?" and next unless -f $period;
    my $lastrun = $script;
    $lastrun =~ s/sh$/lastrun/;
    if ( $self->_need_to_run( $script, $period, $lastrun ) ) {
      $self->fork_run('run_hook_script',$self,$lastrun,$script);
    }
  }
  
}

#effectivelly run a script
sub run_hook_script {
  my ($self,$lastrun,$script) = @_;;
  $self->_do_last_run($lastrun);
  $self->_run_script($script);
  exit 0;  # It is STRONGLY recommended to exit your child process
}


#main while to retrieve hosts from directory structure and fork host alive checking running
sub check_all_hosts_arealive {
  #retrieve watching hosts from meta directory
  say "CHECKING ALL HOSTS";
  my $self = shift;
  my @Hosts;
  my $hosts_dir = join '/', "$Bin/..",'meta';
  while(<$hosts_dir/*>){  
    my $dir = $_;
    my @Path = split m{/},$dir;
    push @Hosts,pop @Path;
  }
  if(@Hosts){
    foreach my $hostname(@Hosts) {
      my $Engines_config = $config->{$hostname}->{deadhost};
      my $sleep = $Engines_config->{interval} // 5;
      sleep $sleep;
      foreach my $engine( keys %{$Engines_config} ){
        next if $engine =~ /interval|recipies/;
        my $econfig = $Engines_config->{$engine};
        $self->fork_run('run_check_alive',$self,$econfig,$hostname,$engine,$Engines_config);
      }
    }
  }
}

#effectivelly check if host are alive 
sub check_host_isalive {
  my ($self,$config,$host,$engine) = @_;
  local $| = 1;
  my $is_alive = 0;
  my $checker = ShellWatch::ServerCheck->new_with_traits( traits => ['ShellWatch::ServerCheck::' . $engine],
                                                                   logger => $self->logger,
                                                                   host => $host,
                                                                   %{$config},
                                                                  );
  $is_alive = $checker->is_alive // 0;
  return $is_alive;
}


#don't know how to avoid
sub run_check_alive {
  my ($self,$econfig,$hostname,$engine,$Engines_config) = @_;
  $0 = 'CHECK_ALIVE';
  if( !$self->check_host_isalive($econfig,$hostname,$engine) ) {
    my @Recipies = ( $Engines_config->{recipies} );
    my $msg = join '-', 'DEADHOST CRITICAL',"Server '$hostname' is apparently unreachable! You MUST check!'";
    $self->_run_notify($msg,$hostname,'serverdead') and exit;
  }
  exit; #absolutelly necessary!
}

sub main::1{}; #ARGH! XGH to solve a strict ref problem. 

#run sub forked...
sub fork_run {
  my ($self,$subname,@parameters) = @_;
  local $| = 1;
  die if !defined $subname or !$subname;
  my $dispatcher = { run => \&{$subname}, };
  my $pid = fork;
  if ($pid) { }
  elsif ($pid == 0) {
    $dispatcher->{run}->(@parameters) and exit;
  }
  else { # Unable to fork
    die "ERROR: Could not fork new process: $!\n\n";
  }
  waitpid ($pid, 0);
}


# Here we go!
sub start {
  my ($self) = @_;
  $self->fork_run('check_all_scripts',$self);
  $self->fork_run('check_all_hosts_arealive',$self);
  return 1;
}





=pod

=encoding utf-8

=head1 NAME

ShellWatch::Daemon - Daemon module for controls running of the hooks's scripts and check if watching hosts are 'alive' or not .

=head1 DESCRIPTION

This module have as goes:

=over

=item * Find hook scripts and run it following the rules defined on hook through web interface.

=item * Check if hosts is alive by ping(Net::Ping) 

=back

=head1 SYNOPSIS
 
 use strict;
 use warnings;
 use ShellWatch::Daemon;

 while (1) {
    eval {
        ShellWatch::Daemon->start;
    };
    warn $@ if $@;
    #...
 }

=head1 GLOBALS

=head2 config

Perl data struct from <SHELLWATCH_HOME>/etc/notifiers.conf

=head1 SUBROUTINES 

=head2 _rrd_file( Str $script_path ) 

Script path have the metric name. All rrd files in shellwatch has the name of metric in your paths. So, 
this subroutine take advantage of that and extract rrd file from it and returns the rrd filename(with path).


=head2 _set_env_by_rrd( Str $rrd_file )

RRD::Simple provides all info about RRD files. This subroutine extract the last value of the variables
that has been used for some script, and puts all of them on $ENV.


=head2 _parse_nagios( Str $message )

Parses a message under 'Nagios standard'( ORIGIN LEVEL - MESSAGE ). Throws an error and returns empty array 
if message is out of this standard. Otherwise return an array with all 'Nagios standard' elements on it.

=head2 _run_notify( Str $output, Str $host, Str $metric )

Check if it's all ok to really run notify. If it is, run it. If not, dies.

=head2 _do_last_run

Updates period file. Period file is a file associated to script. 

=head2 _need_to_run

Check if we need to run, based on the period set by the metric.

=head2 check_all_scripts

Run all hooks saved at /hooks

=head2 check_hosts_is_alive(Str $addr, Int timeout, Int, interval)

Check if host is alive running ping(Net::Ping). If it is, returns true(1).
Otherwise returns 0.

PARAMETERS:

=over

=item addr: host address

=item timeout: DEPRECATED

=item interval: Interval in seconds of check

=back




=head1 AUTHOR

Thiago Rondom L<thiago.rondom@eokoe.com>

=head1 MAINTAINER

Andre Carneiro L<andre.carneiro@eokoe.com>

=head1 LICENSE

Copyright 2015 - Eokoe

=cut

__PACKAGE__->meta->make_immutable;

1;
