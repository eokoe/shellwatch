
package ShellWatch::Util;
use common::sense;
use FindBin qw($Bin);
use File::Path qw/make_path/;
use Exporter 'import';
our @EXPORT_OK = qw(sw_dir);

# our API use the filesystem as database of our hosts and metrics,
# so we need to prevent from 404 access.
sub sw_dir {
    my $dir = join "/", $Bin, "..", @_ ;
    make_path($dir) if !-d $dir;
    return $dir;
}

1;

