
package ShellWatch::Notifier::sms;

use Moose::Role;

has notifier_name => ( is => 'ro', isa => 'Str', default => 'sms' );


has 'responsability' => (
    is       => 'rw',
    isa      => 'Str',
    required => 0        # TODO: /hosts/host/metric
);

has 'delay' => (
    is       => 'rw',
    isa      => 'Int',
    required => 0        # TODO: /hosts/host/metric
);


sub add {}


sub del {}

1;

