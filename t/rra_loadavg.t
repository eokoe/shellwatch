
use Test::More;
use FindBin qw($Bin);
use lib "$Bin/lib";


use_ok('ShellWatch::RRA');

my $metric = ShellWatch::RRA->new_with_traits(
    traits => ['loadavg'],
    host   => 'testing',
    time   => time,
    avg1   => '2.00',
    avg5   => '1.00',
);

ok( $metric->does('ShellWatch::RRA::loadavg'), 'Instance' );


ok( $metric->update, 'update' );

ok( $metric->avg1('1.10'), 'check rw -> avg1');
ok( $metric->avg5('5.00'), 'check rw -> avg5');

ok( $metric->graph, 'graph' );

done_testing();

