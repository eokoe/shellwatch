
use Test::More;
use Test::Exception;
use FindBin qw($Bin);
use lib "$Bin/lib";

my $host = 'testing';
my $cache_dir = $Bin;
$cache_dir =~ s#/t#/cache/$host#;


use_ok('ShellWatch::RRA');

my $metric = ShellWatch::RRA->new_with_traits(
    traits  => ['memory'],
    host    => $host,
    time    => time,
    total   => 1234504454,
    free    => 123412,
    cached  => 123,
    buffers => 12
);


ok( $metric->does('ShellWatch::RRA::memory'), 'Instance' );

ok( -e join('/',$cache_dir,'memory.rrd'),'check_rrd_file_memory' );

lives_ok ( sub { $metric->update },  'expecting to live');

ok( $metric->graph, 'generate graph' );

# ERROR
ok( $metric->total(123), 'check rw ->total' );
ok( $metric->free(500), 'check rw ->free' );
ok( $metric->cached(12), 'check rw -> cached' );
ok( $metric->buffers(12), 'check rw -> buffers' );
throws_ok ( sub { $metric->update },  qr/How can we have more available memory than total/);

done_testing();



