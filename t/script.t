
use Test::More;
use Test::Exception;
use FindBin qw($Bin);
use lib "$Bin/lib";
use Data::Dumper;
use Log::Log4perl;

my $logconf =
'log4perl.rootLogger=DEBUG, LOGFILE
log4perl.appender.LOGFILE=Log::Log4perl::Appender::File
log4perl.appender.LOGFILE.filename=/tmp/shellwatch-test.log
log4perl.appender.LOGFILE.mode=append
log4perl.appender.LOGFILE.layout=PatternLayout
log4perl.appender.LOGFILE.layout.ConversionPattern=[%c] [PID %P] [%H] [%d] %F - %m%n
';

our $logger = Log::Log4perl->get_logger('shellwatch-test');
Log::Log4perl->init( \$logconf );

my $host = 'testing';
my $user = 'tester';

my %Attr_OK = (
  identifier => 'test',
  host       => $host,
  metric     => 'loadavg',
  code       => q[echo "crap"],
  user       => $user,
  logger     => $logger,  
);

#removing test hooks directory
print STDERR `rm -rf $Bin/../hooks/testing/*`;
print STDERR `rm -rf $Bin/../meta/testing/*`;

use_ok('ShellWatch::Script');

use ShellWatch::Script;

dies_ok(
  sub {
    my $script = ShellWatch::Script->new();
  },
  'build_script_object_no_attributes'
);

my $script = ShellWatch::Script->new(%Attr_OK);
ok( ref($script) =~ /ShellWatch::Script/,
  'build_script_object_all_attributes',
);


my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{host};
dies_ok(
  sub {
    my $script = ShellWatch::Script->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_host',
);

my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{identifier};
dies_ok(
  sub {
    my $script = ShellWatch::Script->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_identifier',
);

my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{metric};
dies_ok(
  sub {
    my $script = ShellWatch::Script->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_metric',
);

my $s = ShellWatch::Script->new(%Attr_OK);
my $filename = join '/', $s->base_dir, $host, $user, $s->metric , $s->identifier;
map{unlink $filename . $_;}qw/.sh .period/;
my $ok = $s->add;
my $flag = 1;
foreach my $ext (qw/.sh .period/) {
  $flag = 0 if !-e $filename . $ext;
}

ok(
  $flag,
  'validate_add_method'
);

$ext = '.sh';
$linkname = join '/', $s->link_dir, $host, $user, $s->metric, $s->identifier . $ext;
ok(-e $linkname,'add_link' );


my $ok = $s->remove;
ok($ok,'test_remove_method');

my $link_path   = join '/',$s->link_dir, $host, $user, $s->metric, $s->identifier . $ext;
my $script_path = join '/',$s->link_dir, $user, $s->metric, $s->identifier . $ext;
$ext = '.period',
my $period_path = !-e join '/',$s->base_dir, $user, $s->metric, $s->identifier . $ext;
my $flag = 1;

if( -e $link_path || -e $script_path || -e $period_path ){
  $flag = 0;
}

ok( $flag , 'validate_remove_method' );


$s->add; #Necessary for ScriptMeta tests(lazyness).


done_testing();


