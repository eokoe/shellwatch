
use Test::More;
use Test::Exception;

use FindBin qw($Bin);
use lib "$Bin/lib";

my $host = 'testing';
my $cache_dir = $Bin;
$cache_dir =~ s#/t#/cache/$host#;

use_ok('ShellWatch::RRA');

my $metric = ShellWatch::RRA->new_with_traits(
    traits => ['df'],
    host   => $host,
    time   => time,
    dev    => 'slash',
    available => 30,
);


ok( $metric->does('ShellWatch::RRA::df'), 'Instance' );

my $file = join '/',$cache_dir,'df-slash.rrd';
print STDERR "\nFILE: $file\n";
ok( -e $file ,'check_rrd_file_df' );

ok( $metric->update, 'update' );

ok( $metric->dev('slash'), 'check rw ->dev');
like($metric->rrd_file, qr/slash/, 'check rrd_file');

ok( $metric->available(50), 'check rw ->available');
ok( $metric->total(50), 'check rw ->total');
ok( $metric->used(50), 'check rw ->used');

dies_ok( sub { $metric->available('xxx') } , 'check isa for available');

dies_ok( sub { $metric->total('xxx') } , 'check isa for total');

dies_ok( sub { $metric->used('xxx') } , 'check isa for used');

ok( $metric->rrd_file,'rrd_file');

ok( $metric->graph, 'graph' );



done_testing();

