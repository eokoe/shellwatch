
use Test::More;
use Test::Exception;
use FindBin qw($Bin);
use lib "$Bin/lib";
use Data::Dumper;
use Log::Log4perl;

my $logconf =
'log4perl.rootLogger=DEBUG, LOGFILE
log4perl.appender.LOGFILE=Log::Log4perl::Appender::File
log4perl.appender.LOGFILE.filename=/tmp/shellwatch-test.log
log4perl.appender.LOGFILE.mode=append
log4perl.appender.LOGFILE.layout=PatternLayout
log4perl.appender.LOGFILE.layout.ConversionPattern=[%c] [PID %P] [%H] [%d] %F - %m%n
';

our $logger = Log::Log4perl->get_logger('shellwatch-test');
Log::Log4perl->init( \$logconf );

my $host = 'testing';
my $user = 'tester';

my %Attr_OK = (
  host       => $host,
  user       => $user,
  logger     => $logger,  
);

use_ok('ShellWatch::ScriptMeta');

use ShellWatch::ScriptMeta;

dies_ok(
  sub {
    my $script = ShellWatch::ScriptMeta->new();
  },
  'build_script_object_no_attributes'
);

my $script = ShellWatch::ScriptMeta->new(%Attr_OK);
ok( ref($script) =~ /ShellWatch::ScriptMeta/,
  'build_script_object_all_attributes',
);


my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{host};
dies_ok(
  sub {
    my $script = ShellWatch::ScriptMeta->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_host',
);

my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{identifier};
dies_ok(
  sub {
    my $script = ShellWatch::ScriptMeta->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_identifier',
);

my %Attr_missing_user = %Attr_OK;
delete $Attr_missing{metric};
dies_ok(
  sub {
    my $script = ShellWatch::ScriptMeta->new(%Attr_missing);
  },
  'build_script_object_missing_attribute_metric',
);

my $s = ShellWatch::ScriptMeta->new(%Attr_OK);

ok(keys %{$s->tree},'tree_from_instance_with_attributes_ok');

ok($s->tree_json,'tree_json_from_instance_with_attributes_ok');


done_testing();



