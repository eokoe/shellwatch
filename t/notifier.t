
use Test::More;
use Test::Exception;
use FindBin qw($Bin);
use lib "$Bin/lib";
use Data::Dumper;
use Log::Log4perl;

my $logconf =
'log4perl.rootLogger=DEBUG, LOGFILE
log4perl.appender.LOGFILE=Log::Log4perl::Appender::File
log4perl.appender.LOGFILE.filename=/tmp/shellwatch-test.log
log4perl.appender.LOGFILE.mode=append
log4perl.appender.LOGFILE.layout=PatternLayout
log4perl.appender.LOGFILE.layout.ConversionPattern=[%c] [PID %P] [%H] [%d] %F - %m%n
';

our $logger = Log::Log4perl->get_logger('shellwatch-test');
Log::Log4perl->init( \$logconf );

my $host = 'testing';
my $user = 'tester';

my %Attr_OK = (
  host       => $host,
  metric     => 'loadavg',
  user       => $user,
  logger     => $logger,  
);

use_ok('ShellWatch::Notifier');

use ShellWatch::Notifier;
my $n;
ok( $n = ShellWatch::Notifier->new_with_traits(traits => ['ShellWatch::Notifier::email'],%Attr_OK),'instance_attributes_ok');


#Testing load_from_metric when file doesn't exists
my $ext = '.notifiers';
my $filename =
    join( '/', $n->base_dir, $n->host, $n->metric, $n->metric )
    . $ext;
unlink $filename; #guarantee that file doesn't exists...
my $data = $n->load_config_from_metric;
say STDERR "CONFIG_FROM_METRIC: '" . $n->metric . "' " . Dumper $data;
ok(!defined($data),'load_config_from_metric_without_notifier_file');

#trying add without json
my $ok = $n->add;
say STDERR "ERROR: " . $n->error;
ok(!$ok,'add_notifier_missing_json');



#trying to add with json not ok
$n->json( q[{"CRITICAL":{"email":{"recovery":"0","interval":"10","firstdelay":"10","responsability":"com"}},"WARNING":{"email":{"recovery":"0","firstdelay":"10","interval":"10","responsability":"andre.carneiro@eokoe.com"}},"UNKNOWN":{"email":{"firstdelay":"10","interval":"10","responsability":"andre.carneiro@eokoe.com","recovery":"0"}}}] );
ok(!$n->add);


#trying add with json ok
$n->json( q[{"CRITICAL":{"email":{"recovery":"0","interval":"10","firstdelay":"10","responsability":"andre.carneiro@eokoe.com"}},"WARNING":{"email":{"recovery":"0","firstdelay":"10","interval":"10","responsability":"andre.carneiro@eokoe.com"}},"UNKNOWN":{"email":{"firstdelay":"10","interval":"10","responsability":"andre.carneiro@eokoe.com","recovery":"0"}}}] );
$ok = $n->add;
ok($ok,'add_notifier_with_json');


#trying to notify with parameters ok...
ok($n->notify,'testing_notification_all_parameters_ok');



#Testing missing parameter instance.
my %Attr_missing = %Attr_OK;
delete $Attr_missing{metric};

dies_ok( sub{ ShellWatch::Notifier->new_with_traits(traits => ['ShellWatch::Notifier::email'],%Attr_missing) },'instance_attribute_missing_metric');





done_testing();



